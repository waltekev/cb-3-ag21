#include <stdlib.h>
#include <stdio.h>
#include "minako.h"

int currentToken;
int nextToken;

void program();
void functiondefinition();
void functioncall();
void statementlist();
void block();
void statement();
void ifstatement();
void returnstatement();
void printfstatement();
void type();
void statassignment();
void assignment();
void expr();
void simpexpr();
void term();
void factor();

void error(char* errMsg)  {
    fprintf(stderr, errMsg);
    if(yyin != stdin) {
        fclose(yyin);
    }
    exit(-1);
}

void syntaxError() {
    fprintf(stderr, "Syntaxfehler in Zeile: %d\n", yylineno);
    if(yyin != stdin) {
        fclose(yyin);
    }
    exit(-1);
}

void eat() {
    currentToken = nextToken;
    nextToken = yylex();
}

int isToken(int token) {
    return token == currentToken;
}

void isTokenAndEat(int token) {
    int result = isToken(token);

    if(result == 1) {
        eat();
        return;
    }

    syntaxError();
}

void factor() {
    if(isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN)) {
        eat();
    }else if(isToken(ID)) {
        if(nextToken == '(') {
            functioncall();
        }else {
            eat();
        }
    }else {
        isTokenAndEat('(');
        assignment();

        isTokenAndEat(')');
    }
}

void term() {
    factor();

    while(isToken('*') || isToken('/') || isToken(AND)) {
        eat();
        factor();
    }
}

void simpexpr() {
    if(isToken('-')) {
        eat();
    }

    term();

    while(isToken('+') || isToken('-') || isToken(OR)) {
        eat();
        term();
    }
}

void expr() {
    simpexpr();
    if(isToken(EQ) || isToken(NEQ) || isToken(LEQ) || isToken(GEQ) || isToken(LSS) || isToken(GRT)) {
        eat();
        simpexpr();
    }
}

void assignment() {
    if(isToken(ID) && nextToken == '=') {
        isTokenAndEat(ID);
        isTokenAndEat('=');
        assignment();
    }else if(isToken('-') || isToken(CONST_INT) || isToken(CONST_FLOAT) || isToken(CONST_BOOLEAN) || isToken(ID) || isToken('(')) {
        expr();
    }else {
        syntaxError();
    }
}

void statassignment() {
    isTokenAndEat(ID);
    isTokenAndEat('=');
    assignment();
}

void type() {
    switch(currentToken) {
        case KW_BOOLEAN:
        case KW_FLOAT:
        case KW_INT:
        case KW_VOID: 
            eat();
            break;
        default: 
            syntaxError();
    }
}

void printfstatement() {
    isTokenAndEat(KW_PRINTF);
    isTokenAndEat('(');
    assignment();
    isTokenAndEat(')');
}

void returnstatement() {
    isTokenAndEat(KW_RETURN);
    if(!isToken(';')) {
        assignment();
    }
}

void ifstatement() {
    isTokenAndEat(KW_IF);
    isTokenAndEat('(');
    assignment();
    isTokenAndEat(')');
    block();
}

void statement() {
    int isIF = 0;

    switch(currentToken) {
        case KW_IF:
            ifstatement();
            isIF = 1;
            break;
        case KW_RETURN:
            returnstatement();
            break;
        case KW_PRINTF:
            printfstatement();
            break;
        case ID:
            if(nextToken == '=') {
                statassignment();
            }else if(nextToken == '(') {
                functioncall();
            }else {
                syntaxError();
            }
            break;
        default:
            syntaxError();
    }

    if(!isIF) {
        isTokenAndEat(';');
    }
}

void block() {
    if(isToken('{')) {
        eat();
        statementlist();
        isTokenAndEat('}');
    }else {
        statement();
    }
}

void statementlist() {
    while(isToken('{') ||
          isToken(KW_IF)   ||
          isToken(KW_RETURN)     ||
          isToken(KW_PRINTF)     ||
          isToken(ID)    ) 
    {
        block();
    }
}

void functioncall() {
    isTokenAndEat(ID);
    isTokenAndEat('(');
    isTokenAndEat(')');
}

void functiondefinition() {
    type();
    isTokenAndEat(ID);
    isTokenAndEat('(');
    isTokenAndEat(')');
    isTokenAndEat('{');
    statementlist();
    isTokenAndEat('}');
}

void program() {
    while(isToken(KW_BOOLEAN) ||
          isToken(KW_FLOAT)   ||
          isToken(KW_INT)     ||
          isToken(KW_VOID)    ) 
    {
        functiondefinition();
    }

    isTokenAndEat(EOF);
}

int main(int argc, char **argv) {
    if(argc == 1) {
        yyin = stdin;
    }else {
        yyin = fopen(argv[1], "r");
        if(yyin == NULL) { error("Error: Datei konnte nicht geoeffnet werde.\n"); }
    }

    currentToken = yylex();
    nextToken = yylex();

    program();

    if(yyin != stdin) {
        fclose(yyin);
    }

    return 0;
}